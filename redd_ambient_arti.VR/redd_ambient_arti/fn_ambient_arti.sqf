

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd
	//
	//	Description: Let artillery shot to a given area with given radius an given amount of shells
	//				 To simulate deviation, the targetpoint will be random within the radius
	//				 
	//	Blacklist units when useing headless client: Yes
	//
	//	Example: [[enemyAri_1,enemyAri_2,enemyAri_3],[arti_target_1,arti_target_2,arti_target_3],100,selectRandom [3,4,5],5] remoteExec ["Redd_fnc_ambient_arti",2];
	//		 		 
	//	Parameter(s): 0: ARRAY - Artillery 
	//				  1: ARRAY - Array of arti targets
	//				  2: NUMBER - Radius
	//				  3: NUMBER - Rounds
	//				  4: NUMBER - Time to sleep until next arti mission
	//
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////	

	params ["_enemyArtyArray","_artiTargetArray","_radius","_rounds","_sleep"];	

	//Get random target and position
	_artiTarget = selectRandom _artiTargetArray;
	_centerPos = getPos _artiTarget;
	
	//Exit if there are no guns anymore
	if (_enemyArtyArray isEqualTo []) exitWith {};
	
	_ammo = 0;//init
	//Get enemy ari ammo
	{

		if (alive _x) exitWith 
		{

			_ammo = getArtilleryAmmo [_x] select 0;
			
		};

	}
	foreach _enemyArtyArray;
	
	//Get all rounds to fire
	_allShots = _rounds * (count _enemyArtyArray);

	//Start the counter with zero shots fired, do it Global, otherwise Redd_fnc_AriFireMission doesnt knows the variable
	Redd_arti_shots = 0;
	_decrementRadius = false; //needed for Redd_fnc_AriFireMission but unnecessary in this function

	{
			
		//Check if artillery is alive, otherwise subtract the rounds this artillery has shot
		if ((!alive _x) or ({alive _x} count crew _x == 0)) then {_allShots = _allShots - _rounds};

		//Only run if artillery is alive and has crew
		if ((alive _x) and ({alive _x} count crew _x > 0)) then 
		{
		
			//Get the right ammo, ever artillery should have HE at first magazine
			//Doesent work for mortar with ACE
			_ammo = getArtilleryAmmo [_x] select 0;

			//Spawn the firemission function
			[_x,_centerPos,_radius,_ammo,_rounds,_decrementRadius] spawn Redd_fnc_AriFireMission;
			
			//Wait a while for the next artillery to fire
			uiSleep (0.75 + random 0.5);

		};

	}
	foreach _enemyArtyArray;

	//Wait for all rounds to be shot, than set ammo 1
	waitUntil {sleep 1;Redd_arti_shots == _allShots};
	{
		
		_x setvehicleAmmo 1;
		
	}
	foreach _enemyArtyArray;

	//Time until next ambient fire mission
	//sleep _sleep;

	//[_enemyArtyArray,_artiTargetArray,_radius,_rounds,_sleep] remoteExec ["Redd_fnc_ambient_arti",2];


	true