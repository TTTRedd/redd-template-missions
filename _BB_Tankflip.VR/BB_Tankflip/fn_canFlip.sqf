	
	
	/*
	* Author: BaerMitUmlaut/BlauBär
	* Edit: Redd
	* Checks if the vehicle is upside down.
	*
	* Arguments:
	* 0: _veh <OBJECT>
	*
	* Return Value:
	* None
	*/
	
	params ["_veh"];

	(vectorUp _veh) vectorDotProduct (surfaceNormal position _veh) < 0.8
