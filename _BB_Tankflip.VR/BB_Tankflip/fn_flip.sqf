    
    
    /*
    * Author: BaerMitUmlaut/BlauBär
    * Edit: Redd
    * Moves the vehicle to a free position and flips it.
    *
    * Arguments:
    * 0: _veh <OBJECT>
    *
    * Return Value:
    * None
    */

    params ["_veh"];
    
    hint parseText "<t color='#cf1226' size='2'>Achtung!</t><br />Das Fahrzeug wird in 5 Sekunden umgedreht.";

    uiSleep 5;

    private _position = (getPosATL _veh) findEmptyPosition [0, 100, typeOf _veh];
    _veh setPos _position;