

if !(missionNamespace getVariable ["AriCover",false]) then {

	missionNamespace setVariable ["AriCover",true,true];

	params ["_magazine","_projectile"];

	if ((_magazine isKindOf ["32Rnd_155mm_Mo_shells", configFile >> "CfgMagazines"]) or (_magazine isKindOf ["14Rnd_80mm_rockets", configFile >> "CfgMagazines"])) then {

		_posATL = getPosATL  _projectile;

		while {alive _projectile} do {
			_posATL = getPosATL _projectile;
			uiSleep 0.05;
		};

		_unitArray = nearestObjects [_posATL, ["Man"], 150];

		_unitPos = "";
		{
			_x setVariable ["AriStance",unitPos _x,true];
			_x setUnitPos "DOWN";
		} forEach _unitArray;

		uiSleep 30;

		{
			_x setUnitPos (_x getVariable ["AriStance","AUTO"]);
		} forEach _unitArray;

		missionNamespace setVariable ["AriCover",false,true];
	};	
};