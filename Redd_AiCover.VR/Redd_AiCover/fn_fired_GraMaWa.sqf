

if !(missionNamespace getVariable ["GraMaWaCover",false]) then {

	missionNamespace setVariable ["GraMaWaCover",true,true];

	params ["_projectile"];

	_posATL = getPosATL  _projectile;

	while {alive _projectile} do {
		_posATL = getPosATL _projectile;
		uiSleep 0.05;
	};

	_unitArray = nearestObjects [_posATL, ["Man"], 150];

	_unitPos = "";
	{
		_x setVariable ["GraMaWaStance",unitPos _x,true];
		_x setUnitPos "DOWN";
	} forEach _unitArray;

	uiSleep 30;

	{
		_x setUnitPos (_x getVariable ["GraMaWaStance","AUTO"]);
	} forEach _unitArray;

	missionNamespace setVariable ["GraMaWaCover",false,true];	
};