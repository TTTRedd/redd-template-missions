

//author: Redd

params ["_unit","_type",["_head_headgear",""],["_inventory_headgear",""]];

_unit setVariable ["ACE_isEOD",false,true];
_unit setVariable ["ace_medical_medicClass",0,true];

switch(_type) do
{

	case ("opl"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\gelb.sqf";};
	case ("fac"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\fac.sqf";};
	case ("grpfhr"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\grpfhr.sqf";};
	case ("trpfhr"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\trpfhr.sqf";};
	case ("zf"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\zf.sqf";};
	case ("gren"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\gren.sqf";};
	case ("lmg"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\lmg.sqf";};
	case ("at"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\at.sqf";};
	case ("pio"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\pio.sqf";};
	case ("auf"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\auf.sqf";};
	case ("uav"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\uav.sqf";};
	case ("arzt"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\arzt.sqf";};
	case ("erst"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\erst.sqf";};
	case ("aa"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\erst.sqf";};
	case ("besatzung"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\bro.sqf";};
	case ("hPilot"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\heli.sqf";};
	case ("jPilot"): {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\jet.sqf";};

	default {[_unit,_head_headgear,_inventory_headgear] call compile preProcessFileLineNumbers "loadouts_2\gelb.sqf";};

};