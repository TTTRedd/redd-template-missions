

	//KI SKill
	private _scriptHandle_0 = [] spawn 
	{
		
		{

			if (local leader _x) then 
			{
				
				{
					
					//Set skills
					_x setSkill ["aimingAccuracy",0.4];
					_x setSkill ["aimingShake",0.4];
					_x setSkill ["aimingSpeed",0.75];
					_x setSkill ["commanding",1];
					_x setSkill ["courage",1];
					_x setSkill ["general",0.75];
					_x setSkill ["reloadSpeed",0.75];
					_x setSkill ["spotDistance",1];
					_x setSkill ["spotTime",1];
					
				}
				foreach units _x;
			
			};

		}
		foreach allGroups;

};