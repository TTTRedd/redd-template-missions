/*
 * Author: Sinus // Edit Redd
 * Adds the needed variables and event handler for the statistics at mission end. Execute in initPlayerLocal and initServer.
 *
 * Arguments:
 * None
 *
 * Return Value:
 * None
 *
 * Example:
 * [] call KAT_template_fnc_setUpStatistic;
 *
 * Public: No
 */

// initServer part
if (isServer) then {
    KAT_template_end_fragsOut = 0;
    KAT_template_end_shotsFired = 0;
    KAT_template_end_aiKilled = 0;
    KAT_template_end_aiCount = 0;
    KAT_template_end_bandagesApplied = 0;
    KAT_template_end_pulseChecked = 0;
    KAT_template_end_cprPerformed = 0;
    KAT_template_end_launcherFired = 0;
    KAT_template_end_handGunFired = 0;
    KAT_template_end_vehicleFired = 0;
    KAT_template_end_smokeOut = 0;

    KAT_template_end_aiCount = count (allUnits - playableUnits);
    
    addMissionEventHandler ["EntityKilled", {
        params ["_unit"];

        if (_unit isKindOf "Man") then {
            if (!(isPlayer _unit)) then {
                KAT_template_end_aiKilled = KAT_template_end_aiKilled + 1;
            };
        };
    }];

    ["CAManBase", "Fired", {
        params ["_unit", "_weapon", "_muzzle"];

        if (isPlayer _unit) then {
            if (_muzzle isEqualTo "HandGrenadeMuzzle" || _muzzle isEqualTo "MiniGrenadeMuzzle") then {
                KAT_template_end_fragsOut = KAT_template_end_fragsOut + 1;
            };
            if (_muzzle isEqualTo "SmokeShellMuzzle" || _muzzle isEqualTo "SmokeShellYellowMuzzle" || _muzzle isEqualTo "SmokeShellGreenMuzzle" || _muzzle isEqualTo "SmokeShellRedMuzzle" || _muzzle isEqualTo "SmokeShellPurpleMuzzle" || _muzzle isEqualTo "SmokeShellOrangeMuzzle" || _muzzle isEqualTo "SmokeShellBlueMuzzle") then {
                KAT_template_end_smokeOut = KAT_template_end_smokeOut + 1;
            };
            if (_weapon isEqualTo primaryWeapon _unit) then {
                KAT_template_end_shotsFired = KAT_template_end_shotsFired + 1;
            };
            if (_weapon isEqualTo secondaryWeapon _unit) then {
                KAT_template_end_launcherFired = KAT_template_end_launcherFired + 1;
            };
            if (_weapon isEqualTo handgunWeapon _unit) then {
                KAT_template_end_handGunFired = KAT_template_end_handGunFired + 1;
            };
        };  
    }] call CBA_fnc_addClassEventHandler;

    ["Car", "Fired", {
        params ["_unit", "", "", "", "", "", "", "_vehicle"];

        if (isPlayer _unit && !(_vehicle isEqualTo objNull)) then {
            KAT_template_end_vehicleFired = KAT_template_end_vehicleFired + 1;
        };
    }] call CBA_fnc_addClassEventHandler;
};

// initPlayerLocal part
if (hasInterface) then {
    KAT_template_end_bandagesApplied = 0;
    KAT_template_end_pulseChecked = 0;
    KAT_template_end_cprPerformed = 0;

    ["ace_treatmentSucceded", {
        params ["", "", "", "_className"];

        if (toLower _className in ["fielddressing", "packingbandage", "elasticbandage", "quikclot"]) exitWith {
            KAT_template_end_bandagesApplied = KAT_template_end_bandagesApplied + 1;
        };

        if (toLower _className isEqualTo "checkpulse") exitWith {
            KAT_template_end_pulseChecked = KAT_template_end_pulseChecked + 1;
        };

        if (toUpper _className isEqualTo "CPR") exitWith {
            KAT_template_end_cprPerformed = KAT_template_end_cprPerformed + 1;
        };
    }] call CBA_fnc_addEventHandler;
};