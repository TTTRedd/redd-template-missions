/*
 * Author: Sinus
 * Ends the mission with music, some text and a fade out. Execute global. Local effect.
 * Collects some values from clients and sends the sum back to the clients.
 *
 * Arguments:
 * None
 *
 * Return Value:
 * None
 *
 * Example (call global): 
 * call KAT_template_fnc_endWinNice;
 *
 * Public: Yes
 */

if !(isServer) then {
    [KAT_template_end_bandagesApplied, KAT_template_end_pulseChecked, KAT_template_end_cprPerformed] remoteExec ["KAT_template_fnc_addStatisticValues", 2, false];
} else {
    publicVariable "KAT_template_end_shotsFired";
    publicVariable "KAT_template_end_aiKilled";
	publicVariable "KAT_template_end_aiCount";
    publicVariable "KAT_template_end_fragsOut";
	publicVariable "KAT_template_end_smokeOut";
    publicVariable "KAT_template_end_launcherFired";
    publicVariable "KAT_template_end_handGunFired";
    publicVariable "KAT_template_end_vehicleFired";
    [{
        publicVariable "KAT_template_end_pulseChecked";
        publicVariable "KAT_template_end_bandagesApplied";
        publicVariable "KAT_template_end_cprPerformed";
    }, [], 8] call CBA_fnc_waitAndExecute;

    ["CAManBase", "Fired", {
        params ["_unit", "", "", "", "", "", "_projectile"];
        deleteVehicle _projectile;
        private _oldPos = getPos _unit;
        _unit setPos [0,0,0];
        [{
            params ["_unit", "_oldPos"];
            ["ACE_G_M84" createVehicle (GetPos _unit)] call ace_grenades_fnc_flashbangThrownFuze;
            [{
                params ["_unit", "_oldPos"];
                _unit setPos _oldPos;
            }, [_unit, _oldPos], 1] call CBA_fnc_waitAndExecute;
        }, [_unit, _oldPos], 1] call CBA_fnc_waitAndExecute;
        _unit action ["SWITCHWEAPON", player, player, -1];
    }] call CBA_fnc_addClassEventHandler;
};

[] spawn {
    playMusic "LeadTrack01_F_Tank";
    sleep 3;
    ["<t color='#00ff00'>Mission erfolgreich</t>", 1, 0.8] spawn BIS_fnc_dynamicText;
    sleep 5;
    [
		[(format ["Dauer der Mission: %1 Minuten", floor (time / 60)]), 1, 2],
        [(format ["Anzahl der Spieler: %1", playersNumber playerSide]), 1, 2],
        [(format ["Anzahl der feindlichen Einheiten: %1", KAT_template_end_aiCount]), 1, 2],
		[format ["Feindliche Einheiten vernichtet: %1", KAT_template_end_aiKilled], 1, 3]
    ] spawn BIS_fnc_EXP_camp_SITREP;
    sleep 15;
    [
        [format ["%1 Schüsse aus Sturmgewehren/LMGs/MGs abgegeben", KAT_template_end_shotsFired], 1, 2],
        [format ["%1 Handgranaten geworfen", KAT_template_end_fragsOut], 1, 2],
        [format ["%1 Rauchgranaten geworfen", KAT_template_end_smokeOut], 1, 3]
    ] spawn BIS_fnc_EXP_camp_SITREP;
    sleep 12;
    [
        [format ["%1 Schüsse aus Panzerabwehrwaffen abgegeben", KAT_template_end_launcherFired], 1, 2],
        [format ["%1 Schüsse aus Pistolen abgegeben", KAT_template_end_handGunFired], 1, 2],
        [format ["%1 Schüsse aus Fahrzeugen abgegeben", KAT_template_end_vehicleFired], 1, 3]
    ] spawn BIS_fnc_EXP_camp_SITREP;
    sleep 12;
    [
        [format ["%1 mal Puls gemessen", KAT_template_end_pulseChecked], 1, 2],
        [format ["%1 Bandagen angelegt", KAT_template_end_bandagesApplied], 1, 2],
        [format ["%1 mal CPR durchgeführt", KAT_template_end_cprPerformed], 1, 3]
    ] spawn BIS_fnc_EXP_camp_SITREP;
    sleep 12;
    5 fadeMusic 0;
    ["end1", true, 5] spawn BIS_fnc_endMission;
};