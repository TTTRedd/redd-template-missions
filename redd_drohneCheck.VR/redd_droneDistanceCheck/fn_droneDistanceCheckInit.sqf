///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Disconnects UAV when distance between operator and UAV is greater than 1500 meter
//				You can reconnect by closing the distance to under 1500 meter 
//
//	Example: if (player == <Player Variable Name>) then {[player] spawn Redd_fnc_droneDistanceCheckInit;};
//			Spawn from initPlayerLocal.sqf
//			 		 
//	Parameter(s): 0: OBJECT - Player
//				  
//	Returns: true
//  
///////////////////////////////////////////////////////////////////////////////////////////////////	 

params ["_player"];

_uav = objNull;

while {count allUnitsUAV < 0} do {
	if (Redd_StopDroneDistance) exitWith {};
	uiSleep 10;
};

if (Redd_StopDroneDistance) exitWith {

	hint "Drohnendistanz Script beendet";

	{
		if (_x isKindOf "ITC_Land_B_UAV_AR2i") then {
			_uav = _x;
		};
	} forEach allUnitsUAV;

	_player enableUAVConnectability [_uav,true];
};

{
	if (_x isKindOf "ITC_Land_B_UAV_AR2i" AND _player distance2d _x < 1500) then {
		_uav = _x;
	};
}
forEach allUnitsUAV;

_grp =  groupId group _player;
group _uav setGroupID [format ['%1 AR-2i'], _grp];
_player enableUAVConnectability [_uav,true];

while {_player distance2d _uav < 1500} do {
	if (Redd_StopDroneDistance) exitWith {};
	uiSleep 10;
};

if (Redd_StopDroneDistance) exitWith {

	hint "Drohnendistanz Script beendet";

	{
		if (_x isKindOf "ITC_Land_B_UAV_AR2i") then {
			_uav = _x;
		};
	} forEach allUnitsUAV;
	
	_player enableUAVConnectability [_uav,true];
};

_player connectTerminalToUAV objNull;
_player disableUAVConnectability [_uav,true];

if (alive _player) then {
	[_player] spawn Redd_fnc_droneDistanceCheckInit;
};