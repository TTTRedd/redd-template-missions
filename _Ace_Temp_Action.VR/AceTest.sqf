

	params ["_veh"];

	// Action Var.Name
	private _ace_temp_action =
    [
		//ACE interner Name
        "ace_temp_action",
		//Anzeige Name
        "Ace Temp Action",
		// ??
        "",
		//Params [immer Target, Caller, args] (_args in Params, siehe unten) + Script 
        {   
			//Params inkl. _args
            params ["_target", "_caller", "_args"];
            //_args spawn/call IRGEND EINE FUNKTION;
            hint "Ich bin eine ACE Action";
        },
		//Params  + Condition
        {   
			//Params
            params ["_target", "_caller"];
            true
        },
		// ??
        {},
		//args Array für params
        [_veh]
		//ACE Funktionsaufruf
    ] call ace_interact_menu_fnc_createAction;

	//[Objekt, 0, [Typ], Action Var.Name] call ace_interact_menu_fnc_addActionToObject;
    [_veh,0,["ACE_MainActions"], _ace_temp_action] call ace_interact_menu_fnc_addActionToObject;