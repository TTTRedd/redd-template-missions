	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd, inspired by Joko_Tent
	//
	//	Description:  Let the player play a move and adds a progressbar during disassembling
	//				 
	//	Blacklist units when useing headless client: No
	//
	//	Example: X
	//			 		 
	//	Parameter(s): 0: OBJECT - Player
	//				  			  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////

	(_this select 0) params ["_tent","_unit"];

	_unit playMove "Acts_carFixingWheel";

	[12, [_tent], Redd_fnc_disassembleTent, {_unit switchMove ""}, "Erste-Hilfe Zelt abbauen"] call ace_common_fnc_progressBar;

	true;
