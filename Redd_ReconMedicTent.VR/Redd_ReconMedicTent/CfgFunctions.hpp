    
    
    class Redd_ReconMedicTent
    {
        tag = "Redd";

        class init 
        {

            file="Redd_ReconMedicTent";

            class addTentAction {};
            class findPosition {};
            class buildTent {};
            class disassembleTentProgressBar {};
            class disassembleTent {};

        };

    };
