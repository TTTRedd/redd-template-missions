	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd, inspired by Joko_Tent
	//
	//	Description:  Deletes recon medic tent
	//				 
	//	Blacklist units when useing headless client: No
	//
	//	Example: _action = ["DisassembleReconMedicTent", "Erste-Hilfe Zelt abbauen", "",{true}] call ace_interact_menu_fnc_createAction;
    //    		 [_tent, 0, ["ACE_MainActions"], _action] remoteExec ["ace_interact_menu_fnc_addActionToObject",0,true];
	//			 		 
	//	Parameter(s): 0: OBJECT - Player
	//				  			  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////

	(_this select 0) params ["_tent"];
	
	//Delete the tent
	deleteVehicle _tent;

	//Destroy global variable, so another tent can be build 
	Redd_tent_exists = nil;

	true;
