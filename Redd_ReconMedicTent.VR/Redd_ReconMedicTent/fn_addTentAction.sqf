

    ///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd, inspired by Joko_Tent
	//
	//	Description:  Adds ACE selfinteraction to reconmedic, to build little medic tend behind enemy lines
	//				 
	//	Blacklist units when useing headless client: No
	//
	//	Example: [player] call Redd_fnc_addTentAction;
    //           call from onPlayerRespawn 
	//			 		 
	//	Parameter(s): 0: OBJECT - Player
	//				  			  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////

    params ["_unit"];

    //Adds ACE selfinteraction
    _action = ["ReconMedicTent", "Erste-Hilfe Zelt aufbauen", "",Redd_fnc_findPosition,{isNil "Redd_tent_exists"}] call ace_interact_menu_fnc_createAction;
    [_unit, 1, ["ACE_SelfActions"], _action] call ace_interact_menu_fnc_addActionToObject;

    true;