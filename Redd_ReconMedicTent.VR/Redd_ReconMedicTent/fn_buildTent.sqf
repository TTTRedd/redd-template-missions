    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd, inspired by Joko_Tent
	//
	//	Description:  Build the tend
	//				 
	//	Blacklist units when useing headless client: No
	//
	//	Example: [10, _this, Redd_fnc_buildTent, {}, "Baue Erste-Hilfe Zelt auf"] call ace_common_fnc_progressBar;
	//			 		 
	//	Parameter(s): 0: OBJECT - Player
	//				  			  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////

    (_this select 0) params ["_unit"];

    //Get players position
    _position = (getPos _unit) findEmptyPosition [0, 10, "Land_TentDome_F"];

    //Check if position was found and tell the result to the player, if position was found build the tent
    if (_position isEqualTo []) then 
    {

        hint "Zu wenig Platz";

    } 
    else 
    {
        
        //Create the tent and make it indestructible
        _tent = "Land_TentDome_F" createVehicle _position;
        _tent allowDamage false;

        //Initialise the global variable if tent exists with a simple true
        Redd_tent_exists = true;
        
        //Give the tent the ACE action to be disassembled
        _action = ["ReconMedicTent", "Erste-Hilfe Zelt abbauen", "",Redd_fnc_disassembleTentProgressBar,{Redd_tent_exists}] call ace_interact_menu_fnc_createAction;
        [_tent, 0, ["ACE_MainActions"], _action] call ace_interact_menu_fnc_addActionToObject;
       
        //Set tent to medical facility
        _tent setVariable ["ace_medical_isMedicalFacility", true, true];
        
    };

    true;
