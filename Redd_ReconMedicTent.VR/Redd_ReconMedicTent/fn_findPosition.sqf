    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd, inspired by Joko_Tent
	//
	//	Description:  Looks for empty position to bulid tent
	//				 
	//	Blacklist units when useing headless client: No
	//
	//	Example: _action = ["BuildReconMedicTent", "Erset-Hilfe Zelt aufbauen", "",Redd_fnc_findPosition,{true}] call ace_interact_menu_fnc_createAction;
    //           [_unit, 1, ["ACE_SelfActions"], _action] call ace_interact_menu_fnc_addActionToObject;
	//			 		 
	//	Parameter(s): 0: OBJECT - Player
	//				  			  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////

    params ["_unit"];
    
    //Look for empty position near player
    _position = (getPos _unit) findEmptyPosition [0, 10, "Land_TentDome_F"];
    
    //Check if position was found and tell the result to the player
    if (_position isEqualTo []) then 
    {

        hint "Zu wenig Platz";

    } 
    else 
    {

        //Lets build a tent and play a move during the progress
        _unit playMove "Acts_carFixingWheel";
        [12, [_unit], Redd_fnc_buildTent, {_unit switchMove ""}, "Baue Erste-Hilfe Zelt auf"] call ace_common_fnc_progressBar;

    };

    true;