

	//Spec_Loudout, edit Redd

	_defaultTarn = "winter"; //fleck, fleck_idz, trope, trope_idz, winter, winter_idz
	_isFalli = false; //true, false
	_isGreni = false; //true, false
	_mg_isKingOf = "rnt"; // "rnt", "niarms"

	if(isServer) then 
	{

		{
			
			if(side _x == west) then 
			{

				comment "assign loadout to AI only (excellent for testing purposes)";
				_loadout = _x getVariable ["loadout",""];
				_tarn = _x getVariable ["tarn",_defaultTarn];
				_class = typeOf _x;
				[_x,_loadout,_tarn,_class,_isFalli,_isGreni,_mg_isKingOf] call Redd_fnc_loadout_base;
			
			} 
			else 
			{

				_x removeMagazines "1Rnd_HE_Grenade_shell";

			};

		} 
		foreach  allUnits - allPlayers;
	};

	if(hasInterface) then 
	{
		
		_loadout = player getVariable ["loadout",""];
		_tarn = player getVariable ["tarn",_defaultTarn];
		_class = typeOf player;
		[player,_loadout,_tarn,_class,_isFalli,_isGreni,_mg_isKingOf] call Redd_fnc_loadout_base;

	};

	true