comment "Exported from Arsenal by Redd";

comment "Remove existing items";
removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

comment "Add containers";
_uniform = selectRandom ["BWA3_Uniform_idz_Tropen","BWA3_Uniform3_idz_Tropen"];
player forceAddUniform _uniform;
for "_i" from 1 to 5 do {player addItemToUniform "BWA3_30Rnd_556x45_G36_AP";};

player addVest "BWA3_Vest_Rifleman1_Tropen";
for "_i" from 1 to 2 do {player addItemToVest "BWA3_15Rnd_9x19_P8";};
for "_i" from 1 to 2 do {player addItemToVest "CUP_HandGrenade_M67";};
for "_i" from 1 to 4 do {player addItemToVest "SmokeShell";};
for "_i" from 1 to 6 do {player addItemToVest "ACE_fieldDressing";};
for "_i" from 1 to 6 do {player addItemToVest "ACE_packingBandage";};
for "_i" from 1 to 2 do {player addItemToVest "ACE_tourniquet";};
for "_i" from 1 to 2 do {player addItemToVest "ACE_morphine";};
for "_i" from 1 to 2 do {player addItemToVest "ACE_CableTie";};
for "_i" from 1 to 1 do {player addItemToVest "ACE_Flashlight_MX991";};
for "_i" from 1 to 1 do {player addItemToVest "NVGoggles_OPFOR";};
for "_i" from 1 to 1 do {player addItemToVest "ACE_EntrenchingTool";};
for "_i" from 1 to 1 do {player addItemToVest "DemoCharge_Remote_Mag";};
for "_i" from 1 to 1 do {player addItemToVest "BWA3_optic_NSV600";};
for "_i" from 1 to 2 do {player addItemToVest "Chemlight_green";};
for "_i" from 1 to 2 do {player addItemToVest "rhs_mag_mk84";};

player addBackpack "BWA3_Carryall_Tropen";
for "_i" from 1 to 1 do {player addItemToBackpack "ACE_DefusalKit";};
for "_i" from 1 to 1 do {player addItemToBackpack "ACE_wirecutter";};
for "_i" from 1 to 1 do {player addItemToBackpack "ACE_M26_Clacker";};
for "_i" from 1 to 1 do {player addItemToBackpack "ACE_VMH3";};
for "_i" from 1 to 7 do {player addItemToBackpack "DemoCharge_Remote_Mag";};
for "_i" from 1 to 2 do {player addItemToBackpack "CUP_PipeBomb_M";};

_goggles = selectRandom ["rhs_googles_clear","rhs_googles_black","PBW_shemagh_beige"];
player addGoggles _goggles;

player addHeadgear "rhsusf_opscore_ut";


comment "Add weapons";
player addWeapon "BWA3_G36";
player addPrimaryWeaponItem "BWA3_optic_ZO4x30";
player addPrimaryWeaponItem "BWA3_acc_LLM01_irlaser";
player addWeapon "BWA3_P8";

comment "Add items";
player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "ACE_Altimeter";
player linkItem "TFAR_anprc152";
player linkItem "ItemGPS";

player setVariable ["ACE_isEOD", true];

for "_i" from 1 to 1 do {player addItemToUniform "BWA3_30Rnd_556x45_G36_AP";};