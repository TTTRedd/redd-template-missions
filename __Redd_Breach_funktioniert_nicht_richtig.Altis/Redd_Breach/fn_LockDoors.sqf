////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	AUTHOR: Kex, Edit Redd
//	DATE: 4/25/17
//	VERSION: 1.0
//  DESCRIPTION: Function for lock door module
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Edit Redd
//
//	Example: [this,25] call Redd_fnc_LockDoors
//			 Create "Invisible H" over the building that should have closed doors. 
//			 Call from initline in "Invisible H" or in initServer.sqf with global variable name of "Invisible H"
//
//	Parameter(s): 0: OBJECT - Object to define center
//				  1: NUMBER - Radius
//		
//	Returns: true	
//
///////////////////////////////////////////////////////////////////////////////////////////////////	

		_logic_list = [];
		_sourceObject_list = [];
		_breachableDoors = [];
		_group_logic = createGroup sideLogic;
		_group_logic deleteGroupWhenEmpty true;
		_buildings = nearestObjects [getPos (_this select 0), ["Building"], (_this select 1), true];
		
		{
		
			_building = _x;
			_source_cfg = [(configFile >> "cfgVehicles" >> typeOf _building  >> "AnimationSources"), 0] call BIS_fnc_subClasses;
			
			{
			
				_source = configName _x;
				_content = _source splitString "_";
				
				if (toLower (_content select 2) == "sound") then
				{
				
					_door_name = (_content select [0,2]) joinString "_";
					_trigger_pos = _building modelToWorld (_building selectionPosition (_door_name + "_trigger"));

					_trigger_posHight = _trigger_pos select 2;
					if (_trigger_posHight > 3) exitWith {}; //checks hight, so only doors in basement will be closed
					if (_trigger_pos isEqualTo [0,0,0]) exitWith {}; //checks if doors exist
					
					//just take a random amount of doors to prevent having to much doors
					_randomBool = selectRandom [true,false];
					if (!_randomBool) exitWith {};

					_lock_var = "bis_disabled_" + _door_name;
				
					// close and lock door
					_building animateSource [_source, 0, true];
					_building setVariable [_lock_var, 1, true];
					
					// add door control logic
					_logic = _group_logic createUnit ["module_f", _trigger_pos, [], 0, "CAN_COLLIDE"];
					_logic setVariable ["lock_params", [_building, _lock_var, _trigger_pos, _source], true];
					_logic_list pushBack _logic;
					
					_sourceObject = "Land_ClutterCutter_small_F" createVehicle [0,0,0];
					_sourceObject attachTo [_logic, [0,0,0]];
					_sourceObject_list pushBack _sourceObject;
					_breachableDoors pushBack _sourceObject;
					
					[_sourceObject, ["Deleted", 
					{
						_sourceObject = _this select 0;
						{deleteVehicle _x} forEach (attachedObjects _sourceObject);
						_breachableDoors = _breachableDoors - [_sourceObject];

					}]] 
					remoteExec ["addEventHandler", 2];

				};
				
			} 
			forEach _source_cfg;
			
		} 
		forEach _buildings;