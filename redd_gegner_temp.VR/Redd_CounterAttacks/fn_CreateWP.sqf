

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Creates a simple waypoint on random position in given radius for given group
//				 Type will be "Move"
//			     Behaviour will be "AWARE"
//				 CombatMode will be "RED"
//				 
//	Example: [position this,My_Group_1,500,50] call Redd_fnc_CreateWP
//			 Call from initline 
//			 		 
//	Parameter(s): 0: POSITION - Center position [x,y,z]
//				  1: ARRAY - Group
//				  2: Number - Radius for random position
//				  3: Number - Radius for waypoint completion
//
//	Returns: true
//  
///////////////////////////////////////////////////////////////////////////////////////////////////

params ["_center","_grp","_radius","_cRadius","_combatMode","_waypointCount"];

for "_i" from 0 to _waypointCount do {

	//Calc random position in given radius
	_pos = [(_center select 0) - _radius + (2 * random _radius),(_center select 1) - _radius + (2 * random _radius),0];
	
	//Set simple waypoint
	_wp = _grp addWaypoint [_pos, 0];
	_wp setWaypointType "MOVE";
	_wp setWaypointBehaviour _combatMode;
	_wp setWaypointCombatMode "RED";
	_wp setWaypointCompletionRadius _cRadius;
};

true