

   
    ///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd
	//
	//	Description: Spawns an aircraft on an random position and let it move to a specific trigger area if trigger has fired,
	//				 creates new random number of waypoints and let aircraft patrol after reaching first waypoint
	//				 
	//	Blacklist units when useing headless client: Yes
	//
	//	Example: [thisList,500,[h_1]] call Redd_fnc_Aircraft_Attack
	//			 Create trigger on position where you want triggering units to be attacked, set to "OnlyServer"
	//			 Optional, set condition to "{_x isKindOf "Tank"} forEach thisList", so only Tanks can fire the trigger
    //           Create invisible "h" and set variablename
	//			 Call from OnActivation  
	//			 		 
	//	Parameter(s): 0: ARRAY - Units that fired the trigger
	//				  1: NUMBER - Radius to patrol
    //                2: ARRAY - Varnames from invisible "h"
	//				  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////	

	params ["_bUnitArray","_radius","_spawn_point_array", "_behaviour"];

    //The unit who entered the trigger will be the lucky guy 
	_bUnit = _bUnitArray select 0;
	_pos = getPos _bUnit;
    _unitFound = true;

    _grp = createGroup east;
    _new_spawn_point = selectRandom _spawn_point_array;

    aircraft_array = ["CUP_O_Su25_Dyn_RU","CUP_O_Mi24_V_Dynamic_RU","CUP_O_Mi8_RU"];
    _aircraft_type = selectRandom aircraft_array;
    _count = selectRandom [1,2];

    for "_i" from 0 to _count do {

        //create aircraft and Pilot
        _pilot = _grp createUnit ["CUP_O_RU_Pilot", _new_spawn_point, [], 0, "NONE"];
        _aircraft = createVehicle [_aircraft_type, _new_spawn_point, [], 200, "FLY"];
        _pilot moveInDriver _aircraft; 

    };

    //Spawn function to create hunter loop
	[_bUnit, _radius, _grp, _unitFound, _behaviour] spawn Redd_fnc_HunterLoop;

    true