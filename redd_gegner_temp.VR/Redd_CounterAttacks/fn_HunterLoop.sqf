

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Creates a loop where the position of the _bUnit is updated every 5 minutes with new waypoint creation
//				
//				 
//	Example: [position this, this, 100, oGrpVarName, true] spawn Redd_fnc_HunterLoop;
//			 Call from initline 
//			 		 
//	Parameter(s): 0: Position - Center position [x,y,z]
//				  1: Unit - BluFor unit how fired the trigger
//				  2: Number - Radius for random position
//				  3: Group - OpFor group to hunt the bluFor unit
//				  4: Boolean: true
//
//	Returns: true
//  
///////////////////////////////////////////////////////////////////////////////////////////////////

params ["_bUnit","_radius", "_oGrp", "_unitFound", "_behaviour"];

// Hunter loop
while {_unitFound} do {

	_unitFound = false;
	// Check if any unit in _oGrp is left
	{
		if (alive _x) exitWith {

			_unitFound = true;
		}
	} foreach units _oGrp;

	//Get position of BluFor unit
	_pos = getPos _bUnit;		

	//Add waypoint on position where triggering unit entered the trigger
	_wp_1 = _oGrp addWaypoint [_pos, 0];
	_wp_1 setWaypointType "MOVE";
	_wp_1 setWaypointBehaviour _behaviour;
	_wp_1 setWaypointCombatMode "RED";
	_wp_1 setWaypointTimeout [0, 0, 0];
	_wp_1 setWaypointCompletionRadius 50;

	//Set random number of waypoint to patrol
	_waypointCount = selectRandom [1,2,3,4];

	//Set patrol waypoint
	_handle_waypoint = [_pos,_oGrp,_radius,50,_behaviour,_waypointCount] spawn Redd_fnc_CreateWP;

	//wait for random waypoint to be created
	waitUntil {scriptDone _handle_waypoint};

	//At least set "CYCLE" waypoint so units dont stop at last waypoint
	_pos = getPos _bUnit;
	_wp_2 = _oGrp addWaypoint [_pos, _waypointCount+1];
	_wp_2 setWaypointType "CYCLE";
	_wp_2 setWaypointBehaviour _behaviour;
	_wp_2 setWaypointCombatMode "RED";
	_wp_2 setWaypointTimeout [0, 0, 0];
	_wp_2 setWaypointCompletionRadius 50;

	// Loop every 5 minutes
	uiSleep 300;

	//Check if _bUnit is alive, otherwise exit loop and function
	if (!alive _bUnit) exitWith {};

	//Delete all former waypoints
	while {(count (waypoints _oGrp)) > 0} do {

		deleteWaypoint ((waypoints _oGrp) select 0);
	};
};

true