

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Let units move to a specific trigger area if trigger has fired, deletes all former waypoints,
//				 creates new random number of waypoints from first waypoint and let units patrol after reaching first waypoint
//				 Simulates counterattack on our position, updates every 5 minutes the position for the first waypoint
//				 
//	Blacklist units when useing headless client: Yes
//
//	Example: [thisList,500,[t_1,t_2]] call Redd_fnc_CounterAttack
//			 Create trigger on position where you want triggering units to be attacked, set to "OnlyServer"
//			 Set condition to "{_x isKindOf "Tank"} forEach thisList", so only armored vehicles can fire the trigger
//			 or set condition to "{_x isKindOf "Car"} forEach thisList", so only cars can fire the trigger 
//			 Call from OnActivation  
//			 		 
//	Parameter(s): 0: ARRAY - Units that fired the trigger
//				  1: NUMBER - Radius to patrol
//				  2: ARRAY - Attacking units
//				  
//	Returns: true
//  
///////////////////////////////////////////////////////////////////////////////////////////////////	

params ["_bUnitArray","_radius","_oGrpArray","_behaviour"];

//The first unit who entered the trigger will be the lucky guy 
_bUnit = _bUnitArray select 0;

//Lets go through the array
{
	//Check if any unit is alive in group, otherwise takes another group from array
	_unitFound = false;

	{
		if (alive _x) exitWith {
			_unitFound = true;
		}
	} foreach units _x;

	//Check if we found any unit, otherwise exit function
	if (!_unitFound) exitWith {};

	{
		_x enableAI "PATH";
		_x setUnitPos "AUTO";
	}
	foreach units _x;

	//Delete all former waypoints
	while {(count (waypoints _x)) > 0} do {

		deleteWaypoint ((waypoints _x) select 0);
	};

	//Spawn function to create hunter loop
	[_bUnit, _radius, _x, _unitFound, _behaviour] spawn Redd_fnc_HunterLoop;
}
forEach _oGrpArray;

true