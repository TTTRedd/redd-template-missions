

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Let units move to a specific trigger area if trigger has fired, deletes all former waypoints,
//				 creates new random number of waypoints from first waypoint and let units patrol after reaching first waypoint
//				 
//	Blacklist units when useing headless client: Yes
//
//	Example: [thisList,500,[t_1,t_2]] call Redd_fnc_CounterAttack
//			 Create trigger on position where you want triggering units to be attacked, set to "OnlyServer"
//			 Set condition to "{_x isKindOf "Tank"} forEach thisList", so only armored vehicles can fire the trigger
//			 or set condition to "{_x isKindOf "Car"} forEach thisList", so only cars can fire the trigger 
//			 Call from OnActivation  
//			 		 
//	Parameter(s): 0: ARRAY - Units that fired the trigger
//				  1: NUMBER - Radius to patrol
//				  2: ARRAY - Attacking units
//				  
//	Returns: true
//  
///////////////////////////////////////////////////////////////////////////////////////////////////	

params ["_bUnitArray","_radius","_oGrpArray","_behaviour"];

//The first unit who entered the trigger will be the lucky guy 
_bUnit = _bUnitArray select 0;
_pos = getPos _bUnit;

//Lets go through the array
{
	//Check if any unit is alive in group, otherwise takes another group from array
	_unitFound = false;

	{
		if (alive _x) exitWith {
			_unitFound = true;
		}
	} foreach units _x;

	//Check if we found any unit, otherwise exit function
	if (!_unitFound) exitWith {};

	{
		_x enableAI "PATH";
		_x setUnitPos "AUTO";
	}
	foreach units _x;

	//Delete all former waypoints
	while {(count (waypoints _x)) > 0} do {

		deleteWaypoint ((waypoints _x) select 0);
	};

	//Add waypoint on position where triggering unit entered the trigger
	_wp_1 = _x addWaypoint [_pos, 0];
	_wp_1 setWaypointType "MOVE";
	_wp_1 setWaypointBehaviour _behaviour;
	_wp_1 setWaypointCombatMode "RED";
	_wp_1 setWaypointTimeout [0, 0, 0];
	_wp_1 setWaypointCompletionRadius 50;

	//Set random number of waypoint to patrol
	_waypointCount = selectRandom [1,2,3,4];

	//Set patrol waypoint
	_handle_waypoint = [_pos,_x,_radius,50,_behaviour,_waypointCount] spawn Redd_fnc_CreateWP;

	//wait for random waypoint to be created
	waitUntil {scriptDone _handle_waypoint};

	//At least set "CYCLE" waypoint so units dont stop at last waypoint
	_pos = getPos _bUnit;
	_wp_2 = _x addWaypoint [_pos, _waypointCount+1];
	_wp_2 setWaypointType "CYCLE";
	_wp_2 setWaypointBehaviour _behaviour;
	_wp_2 setWaypointCombatMode "RED";
	_wp_2 setWaypointTimeout [0, 0, 0];
	_wp_2 setWaypointCompletionRadius 50;
}
forEach _oGrpArray;

true