	
	
	//KI SKill
	private _scriptHandle_0 = [] spawn 
	{
		
		{

			if (local leader _x) then 
			{
				
				{
					
					//Set skills
					_x setSkill ["aimingAccuracy",0.4];
					_x setSkill ["aimingShake",0.4];
					_x setSkill ["aimingSpeed",0.75];
					_x setSkill ["commanding",0.75];
					_x setSkill ["courage",0.75];
					_x setSkill ["general",0.75];
					_x setSkill ["reloadSpeed",0.75];
					_x setSkill ["spotDistance",1];
					_x setSkill ["spotTime",1];
					
				}
				foreach units _x;
			
			};

		}
		foreach allGroups;

	};
	
	//Zen
	private _scriptHandle = [] spawn
	{

		//sleep a minute, after a minute we expect that all AI is controlled by the headless client
		sleep 30; //60 in Mission

		//Zen Radius 20
		_radius = 20;
		{
		
			[getPos _x, (units group _x), _radius, true, true] remoteExec ["Ares_fnc_ZenOccupyHouse", _x];

		}
		forEach 
		[
			zen_0
			//GrpFhr VarNamen Einheiten müssen HC Blacklist sein
			
		];

	};


	