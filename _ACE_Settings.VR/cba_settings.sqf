	
	
	force ReddBftEnable = true;
	force ReddBFThideMarker = true;
	force ReddStaticUnitType = true;

	force ace_cookoff_enableAmmobox = false;

	force ace_medical_medicSetting_PAK = 2;
	force ace_medical_medicSetting_SurgicalKit = 2;
	force ace_medical_useLocation_PAK = 2;
	force ace_medical_useLocation_SurgicalKit = 3;

	force ace_refuel_rate = 5;
	
	force ace_weather_enabled = true;

	force acex_headless_enabled = true;

	force ITC_LAND_CIWS = false;

	force TFAR_globalRadioRangeCoef = 1.5;