
params ["_unit"];

comment "Remove existing items";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "Add containers";
_unit forceAddUniform "CFP_U_WorkUniform_M81";
//for "_i" from 1 to 2 do {_unit addItemToUniform "SmokeShell";};
_unit addVest "CFP_ITV_Rifleman_Green";
for "_i" from 1 to 5 do {_unit addItemToVest "ACE_fieldDressing";};
for "_i" from 1 to 2 do {_unit addItemToVest "HandGrenade";};
_unit addItemToVest "hlc_250Rnd_762x51_M_MG3";
_unit addBackpack "SP_Carryall_Green";
for "_i" from 1 to 3 do {_unit addItemToBackpack "hlc_250Rnd_762x51_M_MG3";};
_unit addHeadgear "SP_Mk7Helmet_Green1";
_unit addGoggles "BWA3_G_Combat_black";

comment "Add weapons";
_unit addWeapon "hlc_lmg_MG3";

comment "Add items";

comment "Set identity";
_unit setFace "WhiteHead_01";
_unit setSpeaker "ace_novoice";

[_unit,"USP_PATCH_FLAG_TURKEY"] spawn {params ["_unit","_insignie"];uiSleep 20;[_unit,_insignie] call bis_fnc_setUnitInsignia;};
