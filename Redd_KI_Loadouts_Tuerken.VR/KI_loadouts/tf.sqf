
params ["_unit"];

comment "Remove existing items";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "Add containers";
_unit forceAddUniform "CFP_U_WorkUniform_M81";
for "_i" from 1 to 3 do {_unit addItemToUniform "hlc_20rnd_762x51_b_G3";};
//for "_i" from 1 to 2 do {_unit addItemToUniform "SmokeShell";};
_unit addVest "CFP_ITV_Rifleman_Green";
for "_i" from 1 to 5 do {_unit addItemToVest "ACE_fieldDressing";};
for "_i" from 1 to 5 do {_unit addItemToVest "hlc_20rnd_762x51_b_G3";};
for "_i" from 1 to 2 do {_unit addItemToVest "HandGrenade";};
_unit addHeadgear "SP_Mk7Helmet_Green2";
_unit addBackpack "CUP_B_Kombat_Radio_Olive";
_unit addGoggles "CFP_Neck_Plain2";

comment "Add weapons";
_unit addWeapon "hlc_rifle_g3ka4";
_unit addPrimaryWeaponItem "CUP_optic_CompM4";
_unit addWeapon "Rangefinder";

[_unit,"USP_PATCH_FLAG_TURKEY"] spawn {params ["_unit","_insignie"];uiSleep 20;[_unit,_insignie] call bis_fnc_setUnitInsignia;};