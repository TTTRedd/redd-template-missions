

	params ["_x","_class"];

	switch (_class) do
	{
		
		//AT
		case "CUP_O_RU_Soldier_AT_EMR": {_null = [_x] call compile preProcessFileLineNumbers "KI_loadouts\at.sqf";};
		
		//INF
		case "CUP_O_RU_Soldier_EMR": {_null = [_x] call compile preProcessFileLineNumbers "KI_loadouts\inf.sqf";};
		
		//MG
		case "CUP_O_RU_Soldier_MG_EMR": {_null = [_x] call compile preProcessFileLineNumbers "KI_loadouts\mg.sqf";};
		
		//PZ
		case "CUP_O_RU_Crew_EMR": {_null = [_x] call compile preProcessFileLineNumbers "KI_loadouts\pz.sqf";};

		//TF
		case "CUP_O_RU_Soldier_SL_EMR": {_null = [_X] call compile preProcessFileLineNumbers "KI_loadouts\tf.sqf";};
		
		default {};
		
	};
