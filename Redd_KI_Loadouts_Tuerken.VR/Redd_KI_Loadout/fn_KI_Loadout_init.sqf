

	//based on SPEC Loadout
	if(isServer) then 
	{

		{
			
			if(side _x == east) then 
			{

				comment "assign loadout to AI only (excellent for testing purposes)";
				_loadout = _x getVariable ["loadout",""];
				_class = typeOf _x;
				[_x,_class] call Redd_fnc_ki_loadout_base;
			
			} 
			else 
			{

				_x removeMagazines "1Rnd_HE_Grenade_shell";

			};

			sleep 0.1;

		} 
		foreach  allUnits - allPlayers;
	};
	true