

	params ["_pilot"];

	_vehicle = vehicle _pilot;
	_crew = crew _vehicle;
	_fallisArray = [];

	{
		_iscrew = assignedVehicleRole _x;
		if(count _iscrew > 0) then{
			if((_iscrew select 0) == "Cargo") then {
				_x assignAsCargo _vehicle;
				_fallisArray pushback _x;
			};
		};
	} 
	foreach _crew;

	reverse _fallisArray;
	_vehicle allowDamage false;
	_dir = direction _vehicle;

	uiSleep 1;

	_fallisArray allowGetIn false;

	{
		uiSleep 0.2 + random [0.1,0.15,0.2];
		removeBackpack _x;
		_x disableCollisionWith _vehicle;
		unassignvehicle _x;
		moveout _x;
		_x setDir (_dir + 90);
		_x setvelocity [0,0,-5];
		_x addBackPack "CUP_T10_Parachute_backpack";
	} 
	forEach _fallisArray;

	{
		waitUntil {uiSleep 3;((isTouchingGround _x) || ((position _x select 2) < 1))};
		_x action ["eject", vehicle _x];
	}
	forEach _fallisArray;
